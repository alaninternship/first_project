export interface Itest {
  name: string;
  runTest: () => number;
}

export interface IperiodMeasurement {
  time: number;
  cpu: IcpuAverage;
}

export interface IcpuAverage {
  idle: number;
  total: number;
}

export interface ItestObject {
  title: string;
  tests: Itest[];
}

export interface Inormalize {
  parsedValueToNumber: number;
  enteredValue: string;
}

export interface IresultArray {
  TestName : string;
  AvgMemory : string;
  AvgCPU : string;
  AvgTime : number | string;
  Difference : string;
  ErrTime : string;
}

export interface Iparams {
  iterations: string 
  repeats: string
  PATH: string
}


export interface IprepareParams {
  preparedIterations: number 
  preparedRepeats: number
  PATH: string
}

export interface IsetTotalIdleAndTick {
  totalTick: number ; 
  totalIdle: number;
}