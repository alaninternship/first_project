import { IresultArray } from "../interface/interfaces";
import {OneTestResult} from "./oneTestResult"

export class Result extends OneTestResult {

  constructor(public repeats: number) {
    super(repeats);
  }
  
  private sortResultArray(): void {
    OneTestResult.resultArray.sort((a, b) => {
      return +a.AvgTime - +b.AvgTime;
    });
  }

  private setAdditionalIndicators(): void {
    this.sortResultArray();
    OneTestResult.resultArray[0].Difference = "0 %";
    for (let i = 1; i < OneTestResult.resultArray.length; i++) {
      let percentageOfDifference = +OneTestResult.resultArray[0].AvgTime / +OneTestResult.resultArray[i].AvgTime;
      OneTestResult.resultArray[i].Difference = "+" + Math.round(Math.abs(100 - percentageOfDifference * 100) * 100) /100 + " %";
      OneTestResult.resultArray[i].AvgTime += " ms"
    }
    OneTestResult.resultArray[0].AvgTime += " ms"
  }

  output(): IresultArray[] {
    this.setAdditionalIndicators();
    return OneTestResult.resultArray
    
  }


}
