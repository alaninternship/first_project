import fs from "fs";
import os from "os";
import { IcpuAverage, ItestObject, Inormalize, IsetTotalIdleAndTick} from "../interface/interfaces";
import path from "path";


export function convertToNumber(enteredValue: string): Inormalize {
  const parsedValueToNumber = parseInt(enteredValue);
  return {parsedValueToNumber, enteredValue}
  
}
export function validatePath(pathAfterNormalize: Inormalize):  string {
  if (!isNaN(pathAfterNormalize.parsedValueToNumber)) {
    throw new Error("Incorrect path");
  }
  return pathAfterNormalize.enteredValue;
}

export function validateObject(PATH : string, objectWithTests: ItestObject) {
  if (!fs.existsSync(PATH)) {
    
    throw new Error("Its not a file");
  }
  if (PATH.split(".").pop() !== "js") {
    throw new Error("Incorrect file format! Need a file in '.js' format");
  }
  if (!objectWithTests.title && !objectWithTests.tests) {
    throw new Error("Its not an object with tests");
  }
  return objectWithTests;
}

export function validateIterationsAndRepeats(parsedNumber: Inormalize): number {
  if (isNaN(parsedNumber.parsedValueToNumber)) {
    throw `${parsedNumber.enteredValue} is not a number`;
  } 
  return parsedNumber.parsedValueToNumber;
}

export function normalizePath(PATH: string) : string {
  return path.join(process.cwd(), PATH)
}

export function normalizeIterationsAndRepeats(iterationsOrRepeats : number) : number {
  if (iterationsOrRepeats === 0) {
    throw `There can be 0 number`;
  } else if (iterationsOrRepeats < 0) {
    iterationsOrRepeats = Math.round(Math.abs(iterationsOrRepeats))
  return iterationsOrRepeats
  } return Math.round(iterationsOrRepeats)
}

export function cpuAverage(): IcpuAverage {
  let totalIdle: number = 0,
    totalTick : number = 0;
  let cpus = os.cpus();
  let idleAndTcik = setTotalIdle(totalIdle, totalTick, cpus )
  return { idle: idleAndTcik.totalIdle / cpus.length, total: idleAndTcik.totalTick / cpus.length };
}

function setTotalIdle(totalIdle : number, totalTick : number, cpus :any):IsetTotalIdleAndTick {
  for (let i = 0, len = cpus.length; i < len; i++) {
    let cpu = cpus[i];
    totalTick = setTotalTick(cpu, totalTick)
    totalIdle += cpu.times.idle;
  }
  return {totalTick , totalIdle }

}

function setTotalTick(cpu:any, totalTick : number): number {
  for (let type in cpu.times) {
     totalTick += cpu.times[type];
  }
  return totalTick
}

export async function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}