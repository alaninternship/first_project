import { IresultArray } from "../interface/interfaces";
import { OperationMetrics } from "./operationMetrics";


export class OneTestResult extends OperationMetrics {
  avgMemory: number = 0;
  avgCPU: number = 0;
  avgTime: number = 0;
  errTime: number = 0;
  arrayOfTimes: number[] = [];
  static resultArray: IresultArray[] = [];
  constructor(public repeats: number) {
    super(repeats);
  }


 setTime():void {
    this.counterTime += this.getUsedTime();
    this.arrayOfTimes.push(this.workTime);

  }
  setMemory():void  {
    this.counterMemoryUsage += this.getUsedMemory();

  }
  setCpu():void  {
    this.counterCpu += this.getCPUPercentage();

  }

  setMetrics():void {
    this.setTime();
    this.setMemory();
    this.setCpu();
  }

  calculateAvgResults():void  {
    
    this.avgMemory = Math.round((this.counterMemoryUsage / this.repeats) * 100) / 100;
    this.avgCPU = Math.round((this.counterCpu / this.repeats) * 100) / 100;
    this.avgTime = Math.round((this.counterTime / this.repeats) * 100) / 100;
    for (let i = 0; i < this.repeats; i++) {
      this.errTime += Math.abs(this.avgTime - this.arrayOfTimes[i]);
    }
  }

  setArrayWithResults(): void {
    this.calculateAvgResults()
    const percentageOfErrtime = (this.errTime / this.repeats - this.avgTime) / this.avgTime;
    OneTestResult.resultArray.push({
      TestName : this.testTitle,
      AvgMemory : this.avgMemory + " mb",
      AvgCPU : this.avgCPU + " %",
      AvgTime : this.avgTime,
      Difference : "",
      ErrTime : Math.round(Math.abs(percentageOfErrtime) * 100) /100 + " %" ,
    });
  }
}
