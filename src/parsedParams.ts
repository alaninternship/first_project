import yargs from "yargs";
import { hideBin } from "yargs/helpers";
import {Iparams} from "../interface/interfaces"


export function setParsedParams() : Iparams {
    const argv: any = yargs(hideBin(process.argv)).argv;
    let PATH: string = argv.p || argv.path;
    const [iterations, repeats] : string[] = [argv.i || argv.iterations, argv.r || argv.repeats]
    return {iterations, repeats , PATH}
}

