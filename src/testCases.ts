import { IprepareParams, ItestObject, Iparams, Itest } from './../interface/interfaces';
import { Result } from "./result";
import { convertToNumber, delay } from "./utils";
import { validatePath, validateObject, normalizeIterationsAndRepeats, normalizePath, validateIterationsAndRepeats } from "./utils";


function prepareParams(PATH: string, iterations: string,  repeats: string) : IprepareParams {
  PATH = preparePath(PATH);
  let preparedIterations = prepareIterations(iterations);
  let preparedRepeats = prepareRepeats(repeats);
  return {PATH, preparedIterations, preparedRepeats}
}

function preparePath(PATH: string) : string {
  PATH = validatePath(convertToNumber(PATH));
  PATH = normalizePath(PATH);
  return PATH
}

function prepareIterations(iterations: string) : number {
  let validatedIterations  = validateIterationsAndRepeats(convertToNumber(iterations.toString()))
  let preparedIterations = normalizeIterationsAndRepeats(validatedIterations)
  return preparedIterations
}

function prepareRepeats(repeats: string) : number {
  let validateRepeats = validateIterationsAndRepeats(convertToNumber(repeats.toString()))
  let preparedRepeats = normalizeIterationsAndRepeats(validateRepeats)
  return preparedRepeats
}

function openFile(PATH: string) : ItestObject {
  let testObject : ItestObject = require(PATH);
  testObject = validateObject(PATH, testObject);
  return testObject
}

async function runWarmingUp(tests: Itest[], iterations: number) {
  console.log('Начало прогрева...')
  for (const test of tests) {
    for(let i = 0; i <= iterations; i++){
      test.runTest();
    } 
  }
  console.log("Прогрев завершен. Выполнение тестов...");
  await delay(2000)
}

export async function runBench(
  params: Iparams
) {
  let preparedParams = prepareParams(params.PATH, params.iterations, params.repeats)
  let objectWithTests = openFile(preparedParams.PATH)
  await runWarmingUp(objectWithTests.tests, preparedParams.preparedIterations);
  const result = new Result(preparedParams.preparedRepeats);
  for (const test of objectWithTests.tests) {
    await runTestsWithRepeats(test, preparedParams.preparedIterations, preparedParams.preparedRepeats);
  }

  return result.output();

}

async function runTestsWithRepeats(
  test: Itest,
  iterations: number,
  repeats: number
) {
  const result = new Result(repeats);
  result.setTitle(test.name);
  for (let i = 0; i < repeats; i++) {
    runTestWithIterations(test, iterations, result);
    result.setMetrics();
    await delay(500);
  }
  result.setArrayWithResults();
}

function runTestWithIterations(test, iterations, result) {
  
  result.startMeasurement();
  for (let i = 0; i <= iterations; i++) {
    test.runTest();
  }
  result.endMeasurement();
}
