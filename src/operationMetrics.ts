import { IperiodMeasurement } from "../interface/interfaces";
import { cpuAverage } from "./utils";

export class OperationMetrics  {
  testTitle: string;
  counterTime: number = 0;
  counterMemoryUsage: number = 0;
  counterCpu: number = 0;
  startTime: IperiodMeasurement;
  endTime: IperiodMeasurement;
  workTime: number = 0;

  constructor (public repeats: number) {}
  
  getUsedMemory(): number {
    return process.memoryUsage().heapUsed / 1024 / 1024;
  }

  getUsedTime(): number {
    return this.workTime = this.endTime.time - this.startTime.time;
  }

  startMeasurement(): void {
    this.startTime = {
      time: Date.now(),
      cpu: cpuAverage(),
    };
  }

  endMeasurement(): void {
    this.endTime = {
      time: Date.now(),
      cpu: cpuAverage(),
    };
 
  }
  setTitle(name: string) {
    this.testTitle = name;
  }


  getCPUPercentage(): number {
    let idleDifference: number =
      this.endTime.cpu.idle - this.startTime.cpu.idle;
    let totalDifference: number =
      this.endTime.cpu.total - this.startTime.cpu.total;
    let percentageCPU: number =
      100 - ~~((100 * idleDifference) / totalDifference);
    return percentageCPU;
  }


}
