import { printResultTable } from './resultPrinter';
import { runBench} from "./testCases";
import {setParsedParams} from "./parsedParams"


export const params = setParsedParams();

(async () => {
    const benchResult = await runBench(params);
    printResultTable(benchResult)
})()

