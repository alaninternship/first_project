export function printResultTable(resultArray): void {
    const { Table } = require("console-table-printer");
    const table = new Table();
    addRowsWithColour(table, resultArray)
    table.printTable();
}

function addRowsWithColour(table, resultArray){
  table.addRow(resultArray[0], { color: "green" });
    for (let i = 1; i < resultArray.length; i++) {
      table.addRow(resultArray[i], { color: "red" });
    }
}


